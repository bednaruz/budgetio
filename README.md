# Budgetio #
### Cíl projektu ###
Cílem projektu bylo vytvořit aplikaci, do které si může uživatel ukládat své výdaje a sledovat tak lépe tok svých peněz.

### Popis funkčnosti ###
* Hlavní stránkou je Overview - přehled již vložených útrat podle kategorie, které ukazují pouze posledních 5 vložených položek.
V navigačním sidebaru se dole nachází přístup pro vytvoření nové kategorie a nové položky pro kategorii.
* History - stránka, kde se uživateli zobrazují všechny vložené položky od těch nejaktuálnějších. Uživatel si pomocí From a To
může nastavit rozmezí dní, pro které chce zobrazit vložené položky.
* Recipes - stránka, ve které si uživatel může z vložených položek vytvořit recept. U tohoto eceptu se pak ukazuje celková cena
a recepty se na stránce ukazují od nejlevněšího po nejdražší. Po kliknutí na tlačítko Create recipe se tlačítko změní na search bar,
přes který si uživatel může vyhledat položky z kategorie "Food" a pomocí drap&drop je přenést do sidebaru, kde se položka zaregistruje jako součást receptu.

### Struktura projektu ###
Navigační bar a header jsou templatem pro všechny stránky. Stránky se poté doptávají na html, které vyplňuje zbytek stránky.
Všechny requesty na server probíhají přes javascript, Fetch API s express a cors. Po navrácení response se na základě vrácených dat
vygeneruje html.

Odkazy pro body semestrální práce se nacházejí na odkazu.
