let express = require('express');
let app = express();
let cors = require('cors');
let bodyParser = require('body-parser');
const path = require("path");
let mysql = require('mysql2');

const headers = {'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'}

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// connection configurations
let dbConn = mysql.createConnection({
    host: 'db-mysql-nyc1-16497-do-user-14286315-0.b.db.ondigitalocean.com',
    user: 'doadmin',
    port: 25060,
    password: 'AVNS_QRTesPEjS7Yna9bE5j2',
    database: 'defaultdb'
});

// connect to database
dbConn.connect();

// navigation
app.use("/static", express.static(path.resolve(__dirname, "", "static")));

app.get("/", (req, res) => {
    res.sendFile(path.resolve(__dirname, "", "index.html"));
});

app.get("/history", (req, res) => {
    res.sendFile(path.resolve(__dirname, "", "index.html"));
});

app.get("/recipes", (req, res) => {
    res.sendFile(path.resolve(__dirname, "", "index.html"));
});

// return all categories
app.get('/category', function (req, res) {
    dbConn.query('SELECT * FROM category', function (error, results) {
        if (error) throw error;
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Categories found"
        };
        return res.status(200).send(response.body);
    });
});


// create new category
app.post('/category', function (req, res) {
    let name = req.body.name;
    if (!name) {
        return res.status(400).send({ error: true, message: 'Please provide category' });
    }
    dbConn.query("INSERT INTO category (name) VALUES(?) ", [ name ], function (error, results) {
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Category created"
        };
        return res.status(200).send(response);
    });
});


// return category with specified name
app.get('/category/:name', function (req, res) {
    let category_name = req.body.name;
    if (!category_name) {
        return res.status(400).send({ error: true, message: 'Please provide category name' });
    }
    dbConn.query('SELECT * FROM category WHERE name=(?)', [ category_name ], function (error, results, fields) {
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Category with specified name returned"
        };
        return res.status(200).send(response.body);
    });
});


// create new item (expense)
app.post('/item', function (req, res) {
    let name = req.body.name;
    let price = req.body.price;
    let count = req.body.count;
    let category = req.body.category;

    if (!name || !price || !count || !category) {
        return res.status(400).send({ error: true, message: 'Please provide item information!' });
    }
    dbConn.query("INSERT INTO item (name, price, count, category) VALUES (?,?,?,?)", [name, price, count, category], function (error, results, fields) {
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Item successfully created"
        };
        return res.status(200).send(response);
    });
});


// return all items from specified category
app.get('/item/:category', function (req, res) {
    let category = req.body.name;
    if (!category) {
        return res.status(400).send({ error: true, message: 'Please provide item and its category!' });
    }
    dbConn.query("SELECT * FROM item WHERE category=?", [category], function(error, results) {
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Items found for category"
        };
        return res.status(200).send(response);
    });
});


// return limited number of items from specified category
app.get('/item/limit/:category', function (req, res) {
    let category = req.params.category;
    if (!category) {
        return res.status(400).send({ error: true, message: 'Please provide item and its category!' });
    }
    dbConn.query("SELECT * FROM item WHERE category=? ORDER BY added_date DESC LIMIT ?,?",
        [category, 0, 5], function(error, results) {
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Items ordered by date found"
        };
        return res.status(200).send(response);
    });
});


// return all items from specified date interval
app.post('/item/interval', function (req, res) {
    let from = req.body.from;
    let to = req.body.to;
    if (!from || !to) {
        return res.status(400).send({ error: true, message: 'Please provide item and date range!' });
    }
    dbConn.query("SELECT * FROM item WHERE added_date BETWEEN ? AND ? ORDER BY added_date DESC",
        [from, to], function(error, results) {
            const response = {
                statusCode: 200,
                headers: headers,
                body: JSON.stringify(results),
                mode: 'cors',
                error: false,
                message: "Items from date range found"
            };
            return res.status(200).send(response);
        });
});


// get item by id
app.get('/item/id/:id', function (req,res) {
    let id = req.params.id;
    dbConn.query('SELECT * FROM item WHERE id=?', [id], function (error, results) {
        if (error) throw error;
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Item with specified id found"
        };
        return res.status(200).send(response);
    });
});


// create a new recipe with selected items
app.post('/recipe', function (req, res) {
    let name = req.body.name;
    if (!name) {
        return res.status(400).send({ error: true, message: 'Please provide recipe and items!' });
    }
    dbConn.query("INSERT INTO recipe (name) VALUES (?);",
        [name], function(error, results) {
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Recipe created"
        };
        return res.status(200).send(response);
    });
});

// get last inserted recipe
app.get('/recipe/last', function (req,res) {
    dbConn.query('SELECT id FROM recipe ORDER BY id DESC LIMIT 0,1', function (error, results) {
        if (error) throw error;
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Last recipe returned"
        };
        return res.status(200).send(response);
    });
})

// create recipe items
app.post('/recipe/items', function (req,res) {
    let recipe_id = req.body.recipe_id;
    let items = req.body.items;
    let values = []
    for (const item of items) {
        values.push([recipe_id, item, 1]);
    }
    dbConn.query("INSERT INTO recipe_item (recipe_id, item_id, quantity) VALUES ?",
        [values], function (error, results) {
            const response = {
                statusCode: 200,
                headers: headers,
                body: JSON.stringify(results),
                mode: 'cors',
                error: false,
                message: "Items inserted in recipe"
            };
            return res.status(200).send(response);
        });
});


// return all recipes and their respective items
app.get('/recipe_item', function (req,res) {
    dbConn.query('SELECT * FROM recipe_item', function (error, results) {
        if (error) throw error;
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Recipe items found"
        };
        return res.status(200).send(response);
    });
});


// get recipe with specified id
app.get('/recipe/:id', function (req,res) {
    let id = req.params.id;
    dbConn.query('SELECT * FROM recipe WHERE id=?', [id], function (error, results) {
        if (error) throw error;
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Recipe with id found"
        };
        return res.status(200).send(response);
    });
});


// search for items which contain specified string
app.get('/item/guess/:letters', function (req,res) {
    let letters = req.params.letters;
    let query_string = "SELECT * FROM item WHERE category='Food' AND name LIKE " + "\'%" + letters + "%\'";
    dbConn.query(query_string, function (error, results) {
        if (error) throw error;
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(results),
            mode: 'cors',
            error: false,
            message: "Items from food category with specified string found"
        };
        return res.status(200).send(response);
    });
});

// set port
app.listen(3000, function () {
    console.log('Node app is running on port 3000');
});
module.exports = app;
