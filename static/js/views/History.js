import AbstractView from "./AbstractView.js";

const ENDPOINT_SERVER = "http://127.0.0.1:3000";

const Months = {
  "01" : "January",
  "02" : "February",
  "03" : "March",
  "04" : "April",
  "05" : "May",
  "06" : "June",
  "07" : "July",
  "08" : "August",
  "09" : "September",
  "10" : "October",
  "11" : "November",
  "12" : "December"
}
/*
 the purpose of this class is to display all inserted items from the start date to the end date
 starting with the biggest interval
*/
export default class extends AbstractView {
  constructor(params) {
    super(params);
    this.setTitle("History");
    this.setHeading(super.title);
  }

  async getHtml() {
    // get today's date and format it
    let date = new Date();
    let date_string = this.transform_date(date);

    // get inserted items from now to the first item
    let items = await this.getData(date_string, "1001-01-01 00:00:01");
    let items_html = this.generateHTMLFromData(items);
    return  "<div id='history-container'>" +
              "<div class='date-interval-container'>" +
                "<label for='start_date'>From </label>" +
                "<input type='text' name='start_date' class='start_date' placeholder='First expense' value='1001-01-01'>" +
                "<div class='filler-div'></div>" +
                "<label for='end_date'>To </label>" +
                "<input type='text' name='end_date' class='end_date' placeholder='Today'>" +
                "<div class='filler-div'></div>" +
                "<button type='button' id='show_history'>Show me</button>" +
              "</div>" + items_html +
            "</div>";
  }

  // get inserted items from now to the first item
  async getData(start_date, end_date) {
    return await fetch(ENDPOINT_SERVER + "/item/interval", {
      headers: {
        "Accept": "application/json; charset=UTF-8",
        "Response-type": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Access-Control-Allow-Origin": "*"
      },
      method: "POST",
      body: JSON.stringify({
        from: end_date,
        to: start_date
      }),
      mode: "cors"
    }).then((resp) => resp.json());
  }

  generateHTMLFromData(items) {
    let html = "<div class='history-content'>";
    items = JSON.parse(items.body);
    // generate html for given items
    if (items.length > 0) {
      let year_month = "";
      let last_year_month = year_month;
      html += "<div class='empty-div'><div>" // div to fill the space
      // go through all the items
      for (const item of items) {
        // take the date part from timestamp
        year_month = item["added_date"].substring(0,7);
        // do this only if this is the first month container in dom or the month differes from the last one
        if (year_month !== last_year_month) {
          console.log(year_month + " " + last_year_month)
          last_year_month = year_month;
          // take year from timestamp
          let year = item["added_date"].substring(0,4);
          // take date from timestamp
          let month = Months[item["added_date"].substring(5,7)];
          // generate container html for a month in a year starting from the closest to present
          html += "</div></div>" +
              "<div class='grid-item'>" +
                "<div class='month-wrapper'>" +
                  "<div class='month-header'>" +
                    "<strong>" +
                      month + " " + year +
                    "</strong>" +
                  "</div>"
        }
        // generate html for every item for that month with its name, date it was created and price
          html += "<div class='record-container'>" +
                    "<b>" + item["name"] + "</b>" +
                    "<small>" + item["added_date"].substring(0,10) + "</small>" +
                    "<strong>" + item["price"] + " Kč</strong>" +
                  "</div>"
      }
      html += "</div></div></div>"
    } else {
      html += "<h3 class='center'>There are no items yet</h3></div>"
    }

    return html;
  }

  // transform date to a usable format that can be stored in database
  transform_date(date) {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    let hour = date.getHours();
    let minute = date.getMinutes();
    let second = date.getSeconds();

    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }

    if (hour < 10) {
      hour = '0' + hour;
    }
    if (minute < 10) {
      minute = '0' + minute;
    }
    if (second < 10) {
      second = '0' + second;
    }

    return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
  }
}





