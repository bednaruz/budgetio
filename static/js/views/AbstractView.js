export default class {
  constructor(params) {
    this.params = params;
  }

  setHeading() {
    const heading = document.getElementById("heading");
    heading.innerHTML = this.title;
  }

  setTitle(title) {
    this.title = title;
    document.title = title;
  }

  async getHtml() {
    return "";
  }

  async getData(start_date, end_date) {
    return null;
  }

  generateHTMLFromData(items) {
    return "";
  }

  transform_date(date) {
    return "";
  }
}