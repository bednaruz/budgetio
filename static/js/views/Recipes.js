import AbstractView from "./AbstractView.js";

const ENDPOINT_SERVER = "http://127.0.0.1:3000";

// in this page, the user can create a recipe from items from the Food category
// the recipes than show from the least expensive to the most expensive one
export default class extends AbstractView {
  constructor(params) {
    super(params);
    this.setTitle("Recipes");
    this.setHeading(super.title);
  }

  async getHtml() {
    let items = await this.getRecipeItems();
    return this.generateHTMLFromData(items);
  }

  // get all recipe items
  async getRecipeItems() {
    // get relations between recipe and items
    let items = await fetch(ENDPOINT_SERVER + "/recipe_item", {
      headers: {
        "Accept": "application/json; charset=UTF-8",
        "Response-type": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Access-Control-Allow-Origin": "*"
      },
      method: "GET",
      mode: "cors"
    }).then((resp) => resp.json());
    // make map: recipe_id -> items
    const recipe_items_map = new Map();
    items = JSON.parse(items.body);
    for(const item of items) {
      if (!recipe_items_map.has(item.recipe_id)) {
        let recipe_name = await this.getRecipeNameById(item.recipe_id);
        recipe_items_map.set(item.recipe_id, [JSON.parse(recipe_name.body)[0].name, []]);
      }
      let returned_item = await this.getItemById(item.item_id);
      recipe_items_map.get(item.recipe_id)[1].push(JSON.parse(returned_item.body));
    }
    return recipe_items_map;
  }

  // get recipe with specified id
  async getRecipeNameById(id) {
    return await fetch(ENDPOINT_SERVER + "/recipe/" + id, {
      headers: {
        "Accept": "application/json; charset=UTF-8",
        "Response-type": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Access-Control-Allow-Origin": "*"
      },
      method: "GET",
      mode: "cors"
    }).then((resp) => resp.json());
  }

  // get item by specified id
  async getItemById(id) {
    return await fetch(ENDPOINT_SERVER + "/item/id/" + id, {
      headers: {
        "Accept": "application/json; charset=UTF-8",
        "Response-type": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Access-Control-Allow-Origin": "*"
      },
      method: "GET",
      mode: "cors"
    }).then((resp) => resp.json());
  }

  generateHTMLFromData(recipe_items_map) {
    // generate html for the top part of the page (everything except the recipes themselves)
    let html =
        "<div id='recipes-container'>" +
          "<div class='main-wrapper'>" +
            "<div id='searchbar'>" +
              "<button id='create-recipe-button' style='display: block'>" +
                "<img src='static/icons/add.svg' class='menu_icon' alt='Create recipe icon'/><b>Create recipe</b>" +
              "</button>" +
              "<div id='search-container' style='display: none'>" +
                "<div class='search-food'>" +
                  "<input id='food-input' type='text' placeholder='Search for a food item...'>" +
                    "<div class='result-box'>" +
                    "</div>" +
                    "<div class='icon'><i class='fas fa-search'></i></div>" +
                "</div>" +
              "</div>" +
            "</div>" +
            "<div id='recipes-wrapper'>";

    // compute the complete prices from the items of each recipe
    let recipe_price_map = new Map();
    recipe_items_map.forEach((value,key) => {
      recipe_price_map.set(key, 0);
      for (const item of value[1]) {
        recipe_price_map.set(key, recipe_price_map.get(key) + item[0].price);
      }
    })


    // sort the recipes from the lowest price to the highest
    let recipes_sorted = new Map([...recipe_price_map.entries()].sort((a, b) => a[1] - b[1]));
    // add the recipes to html in that order, each item has name and price
    recipes_sorted.forEach((value, key) => {
      let recipe_item = recipe_items_map.get(key);
      html += "<div class='recipe'>" +
                "<h3>" + recipe_item[0] + "</h3>";
                    for (const item of recipe_item[1]) {
                      html += "<div class='food-item'>" +
                                "<h3>" + item[0].name +"</h3>" +
                                "<strong>" + item[0].price + "Kč</strong>" +
                              "</div>";
                    }
      html += "</div>";
    });
    html += "</div>" +
          "</div>" +
        "</div>";
    return html;
  }
}