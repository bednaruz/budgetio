import AbstractView from "./AbstractView.js";

const ENDPOINT_SERVER = "http://127.0.0.1:3000";

// the purpose of this page is to show an overview of categories and inserted items
// for each category there is only the last 5 most recent items shown
// categories with no items don't show
export default class extends AbstractView {
  constructor(params) {
    super(params);
    this.setTitle("Overview");
    this.setHeading(super.title);
  }

  async getHtml() {
    let categories = await getCategoriesForOverview();
    // put them in grid items and respective elements
    let categories_html = generateHTMLCategories(categories);
    // add the grid items in existing html
    let html = "<div id='overview-container' class='grid-container'>";
    for (const category of categories_html) {
      html += category;
    }
    html += "</div>";
    return html;
  }
}

async function getCategoriesForOverview() {
  let categories = await getAllCategories();
  let array = []
  for (const category of categories) {
    array.push(category["name"]);
  }
  let categories_with_items = [];
  for (const category of array) {
    // get only 5 most recent items for each category
    let items = await getItemsOfCategoryWithLimit(category);
    items = JSON.parse(items.body);
    if (items.length > 0) {
      categories_with_items.push([category, items]);
    }
  }
  return categories_with_items;
}

async function getAllCategories() {
  return await fetch(ENDPOINT_SERVER + "/category", {
    headers: {
      "Accept": "application/json; charset=UTF-8",
      "Response-type": "application/json; charset=UTF-8",
      "Access-Control-Allow-Origin": "*"
    },
    method: "GET",
    mode: "cors"
  }).then((resp) => resp.json());
}

function generateHTMLCategories(categories) {
  let categories_html = [];
  let html = "";
  // create wrapper for each category
  for (const category of categories) {
    html = "<div class='grid-item'>" +
                  "<div class='category-wrapper'>" +
                      "<h3 class='category-heading'>" + category[0] + "</h3>"
    // generate html for items in that category with its name, date it was created and price
    for (const item of category[1]) {
      html += "<div class='record-container'>" +
                "<b>" + item["name"] + "</b>" +
                "<small>" + item["added_date"].substring(0,10) + "</small>" +
                "<strong>" + item["price"] + " Kč</strong>" +
              "</div>"
    }
    html +=   "</div>" +
          "</div>";
    categories_html.push(html);
  }
  return categories_html;
}

// get only 5 most recent items for each category
async function getItemsOfCategoryWithLimit(category) {
  return await fetch(ENDPOINT_SERVER + "/item/limit/" + category, {
    headers: {
      "Accept": "application/json; charset=UTF-8",
      "Response-type": "application/json; charset=UTF-8",
      "Access-Control-Allow-Origin": "*"
    },
    method: "GET",
    mode: "cors"
  }).then((resp) => resp.json());
}