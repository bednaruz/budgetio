
const ENDPOINT_SERVER = "http://127.0.0.1:3000";
let dropdown_set = false;

// buttons from navigation bar for creating stuff
const item_dialog = document.getElementById('add_item');
const category_dialog = document.getElementById('add_category');

// add and cancel buttons that show and hide the dialogs
const item_dialog_show_button = document.getElementById('show_item');
const item_dialog_hide_button = document.getElementById('hide_item');
const category_dialog_show_button = document.getElementById('show_category');
const category_dialog_hide_button = document.getElementById('hide_category');

// submit buttons for creation of category and item
const create_category_button = document.getElementById('create_category');
const create_item_button = document.getElementById('create_item');

// dropdown showcasing all the inserted categories
const category_dropdown = document.getElementById('categories');

// show add item dialog and fill the category dropdown with categories from database
item_dialog_show_button.addEventListener('click', async (e) => {
  e.preventDefault();
  // fill the category dropdown with existing categories
  if (!dropdown_set) {
    await getAllCategories()
        .then((response) => {
          for (let i = 0; i < response.length; i++) {
            let elem = document.createElement('option');
            elem.value = response[i]["name"];
            elem.innerText = response[i]["name"];
            category_dropdown.appendChild(elem);
          }
        });
    dropdown_set = true;
  }

  item_dialog.showModal();
})

// cancel for add item dialog
item_dialog_hide_button.addEventListener('click', (e) => {
  e.preventDefault();
  item_dialog.close();
})

// show the add category dialog
category_dialog_show_button.addEventListener('click', (e) => {
  e.preventDefault();
  category_dialog.showModal();
})

// cancel for the add category dialog
category_dialog_hide_button.addEventListener('click', (e) => {
  e.preventDefault();
  category_dialog.close();
})

// if the user clicks somewhere else when the dialog is opened, the dialogs close
document.addEventListener('click', (e) => {
  e.preventDefault();
  closeItemDialog(e);
  closeCategoryDialog(e);
})

// create category and write it to database
create_category_button.addEventListener('click', (e) => {
  e.preventDefault();
  let form = document.getElementById('create_category_form');
  let result = createCategory(form);
  category_dialog.close();
})

// create item and write it to database
create_item_button.addEventListener('click', (e) => {
  e.preventDefault();
  let form = document.getElementById('create_item_form');
  let result = createItem(form);
  item_dialog.close();
  location.reload();
})

// create category and write it to server
async function createCategory(form) {
  let form_data = new FormData(form);
  let category_name = form_data.get("category_name");

  // check if category name exists
  let category_exists = await getCategoryByName(category_name);
  if (!category_exists.length) {
    return await fetch(ENDPOINT_SERVER + "/category", {
      method: "POST",
      headers: {
        "Accept": "application/json; charset=UTF-8",
        "Response-type": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Access-Control-Allow-Origin": "*"
      },
      body: JSON.stringify({
        name: category_name
      }),
      mode: "cors"
    }).then((resp) => resp.text().then((body) => body));
  } else {
    alert("This category already exists! Try another one!");
  }
}

function closeItemDialog(e) {
  if (!e.target.contains(item_dialog)) return;
  item_dialog.close();
}

function closeCategoryDialog(e) {
  if (!e.target.contains(category_dialog)) return;
  category_dialog.close();
}

async function getAllCategories() {
  return await fetch(ENDPOINT_SERVER + "/category", {
    headers: {
      "Accept": "application/json; charset=UTF-8",
      "Response-type": "application/json; charset=UTF-8",
      "Access-Control-Allow-Origin": "*"
    },
    method: "GET",
    mode: "cors"
  }).then((resp) => resp.json());
}

async function getCategoryByName(name) {
  return await fetch(ENDPOINT_SERVER + "/category/" + name, {
    headers: {
      "Accept": "application/json; charset=UTF-8",
      "Response-type": "application/json; charset=UTF-8",
      "Access-Control-Allow-Origin": "*"
    },
    method: "GET",
    mode: "cors"
  }).then((resp) => resp.json());
}

async function createItem(form) {
  let form_data = new FormData(form);
  let date_object = new Date();

  let name = form_data.get("item_name");
  let price = form_data.get("item_price");
  let count = form_data.get("item_count");
  let category = form_data.get("categories");
  let date = `${date_object.getFullYear()}-${date_object.getMonth()}-${date_object.getDate()}`

  const response = await fetch(ENDPOINT_SERVER + "/item", {
    method: "POST",
    headers: {
      "Accept": "application/json; charset=UTF-8",
      "Response-type": "application/json; charset=UTF-8",
      "Content-Type": "application/json; charset=UTF-8",
      "Access-Control-Allow-Origin": "*"
    },
    body: JSON.stringify({
      name: name,
      price: price,
      count: count,
      category: category,
      date: date
    }),
    mode: "cors"
  }).then((resp) => resp.text().then((body) => body));
}

async function getItemsByCategory(category) {
  return await fetch(ENDPOINT_SERVER + "/item/" + category, {
    headers: {
      "Accept": "application/json; charset=UTF-8",
      "Response-type": "application/json; charset=UTF-8",
      "Access-Control-Allow-Origin": "*"
    },
    method: "GET",
    mode: "cors"
  }).then((resp) => resp.json());
}



