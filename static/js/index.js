import Overview from "./views/Overview.js";
import Recipes from "./views/Recipes.js";
import History from "./views/History.js";

const ENDPOINT_SERVER = "http://127.0.0.1:3000";
const pathToRegex = path => new RegExp("^" + path.replace(/\//g, "\\/").replace(/:\w+/g, "(.+)") + "$");

const getParams = match => {
  const values = match.result.slice(1);
  const keys = Array.from(match.route.path.matchAll(/:(\w+)/g)).map(result => result[1]);

  return Object.fromEntries(keys.map((key, i) => {
    return [key, values[i]];
  }));
};

const navigateTo = url => {
  history.pushState(null, null, url);
  router();
};

// define the pages routes
const router = async () => {
  const routes = [
    { path: "/", view: Overview },
    { path: "/history", view: History },
    { path: "/recipes", view: Recipes }
  ];

  // test each route for potential match
  const potentialMatches = routes.map(route => {
    return {
      route: route,
      result: location.pathname.match(pathToRegex(route.path))
    };
  });

  let match = potentialMatches.find(potentialMatch => potentialMatch.result !== null);

  if (!match) {
    match = {
      route: routes[0],
      result: [location.pathname]
    };
  }

  const view = new match.route.view(getParams(match));

  // get html from the AbstractView pages
  document.querySelector("#app").innerHTML = await view.getHtml();

  // if the current location is History
  if (view.title === "History") {
    // datepicker handlers on date inputs
    $(function(){
      $('.start_date').on('click', function() {
        $(this).datepicker({showOn:'focus', dateFormat: 'yy-mm-dd' }).focus();
      });
      $('.end_date').on('click', function() {
        $(this).datepicker({showOn:'focus', dateFormat: 'yy-mm-dd' }).focus();
      });
    });

    // look into database and show the items from history with the chosen from and to date
    document.getElementById('show_history').addEventListener('click', async () => {
      document.querySelector(".history-content").remove();

      let start_date = $(".start_date").val();
      let start_date_string, date;
      // use default start date if it isn't set
      if (start_date === undefined) {
        start_date_string = "1001-01-01 00:00:01";
      } else {
        let date = new Date(start_date);
        start_date_string = view.transform_date(date);
      }

      let end_date = $(".end_date").val();
      let end_date_string;
      // use the default end date if it isn't defined
      if (end_date === undefined) {
        date = new Date();
      } else {
        date = new Date(end_date);
      }
      end_date_string = view.transform_date(date);

      // get the items for that date interval
      let items = await view.getData(end_date_string, start_date_string);
      let items_html = view.generateHTMLFromData(items);

      document.getElementById('history-container').insertAdjacentHTML('beforeend', items_html);
    });
  } else if (view.title === "Recipes") {  // if we are in the recipes section
    // on create recipe button - create the sidebar to with you can drag the items and write recipe name
      document.getElementById('create-recipe-button').addEventListener('click', () => {
        let root = document.getElementById('recipes-container');
        let sidebar = document.createElement('div');
        sidebar.setAttribute('id', 'recipes-sidebar');

        let recipe_name_div = document.createElement('div');
        recipe_name_div.setAttribute('id','recipe-name-div');

        // the check button to save the recipe to database
        let save = document.createElement('button');
        save.setAttribute('id', 'save-recipe');
        save.innerHTML = "<img src='static/icons/check.svg' alt='Check icon'/>";

        // input field for the recipe name
        let recipe_name = document.createElement('input');
        recipe_name.setAttribute('type', 'text');
        recipe_name.setAttribute('placeholder', 'Groceries');
        recipe_name.setAttribute('required','');

        // cancel creating the recipe
        let cancel = document.createElement('button');
        cancel.setAttribute('id', 'cancel-recipe');
        cancel.innerHTML = "<img src='static/icons/cross.svg' alt='Cross icon'/>";

        // insert the confirmation buttons and the recipe name input field together
        recipe_name_div.appendChild(cancel);
        recipe_name_div.appendChild(recipe_name);
        recipe_name_div.appendChild(save);

        // create container to which drop the items
        let droppable_container = document.createElement('div');
        droppable_container.setAttribute('id', 'droppable-container');
        droppable_container.setAttribute('class', 'droppable-show');
        droppable_container.setAttribute('ondrop','drop_handler(event)');
        droppable_container.setAttribute('ondragover','dragover_handler(event)');
        droppable_container.innerText = 'Drop item here';

        // attach the created elements
        root.appendChild(sidebar);
        sidebar.appendChild(recipe_name_div);
        sidebar.appendChild(droppable_container);

        // change the create recipe button to a search input
        let search_food = document.getElementById('search-container');
        search_food.style.display = 'block';
        let me = document.getElementById('create-recipe-button');
        me.style.display = 'none';

        // after cancel return everything to the starting state
        cancel.addEventListener('click', () => {
          let search_food = document.getElementById('search-container');
          search_food.style.display = 'none';
          let me = document.getElementById('create-recipe-button');
          me.style.display = 'block';
          root.removeChild(sidebar);
        });

        // save the recipe to database and return everything to the starting state, play sound
        save.addEventListener('click', async () => {
          let name = recipe_name.value;
          let possible_items = droppable_container.querySelectorAll('li');
          let items = possible_items == null ? [] : possible_items;
          let ids = [];
          for (const item of items) {
            ids.push(item.id[0])
          }
          await createRecipe(name, ids);
          let audio = document.getElementById("audio");
          await audio.play();
          setTimeout(reloadPage,3000);
        });
      });

      // search bar for the food items
      const search_input = document.querySelector(".search-food");
      const input = search_input.querySelector("#food-input");
      const result_box = search_input.querySelector(".result-box");

      // on every keyup in the searchbar - get the respective items from database and append them to html
      input.addEventListener('keyup', async (e) => {
        let letters = e.target.value;
        let html_items = "";
        if (letters) {
          // send request for part of the name - item
          let food_items = await getItemsFromLetters(letters);
          food_items = JSON.parse(food_items.body);
          for (const item of food_items) {
            html_items += "<li id=" + "\'" + item.id + "-food-item\'" + " draggable='true' ondragstart='dragstart_handler(event)'>" + item.name + "</li>";
          }

          search_input.classList.add("active-search");
          result_box.innerHTML = "<ul>" + html_items + "</ul>";
        } else {
          search_input.classList.remove("active-search");
        }
      });
  }
};

function reloadPage() {
  location.reload();
}

async function createRecipe(name, items) {
  // create the recipe, then get its id created in database, insert recipe items for that id
  if (!name || !items || items.length === 0) {
    alert("The recipe must have a name and at least one item!");
  } else {
    return await fetch(ENDPOINT_SERVER + "/recipe", {
      method: "POST",
      headers: {
        "Accept": "application/json; charset=UTF-8",
        "Response-type": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Access-Control-Allow-Origin": "*"
      },
      body: JSON.stringify({
        name: name
      }),
      mode: "cors"
    }).then(async (resp) => await resp.text())
        .then(async data => {
          return await fetch(ENDPOINT_SERVER + "/recipe/last", {
            method: "GET",
            headers: {
              "Accept": "application/json; charset=UTF-8",
              "Response-type": "application/json; charset=UTF-8",
              "Content-Type": "application/json; charset=UTF-8",
              "Access-Control-Allow-Origin": "*"
            },
            mode: "cors"
          }).then((resp) => resp.json())
              .then(async data => {
                return await fetch(ENDPOINT_SERVER + "/recipe/items", {
                  method: "POST",
                  headers: {
                    "Accept": "application/json; charset=UTF-8",
                    "Response-type": "application/json; charset=UTF-8",
                    "Content-Type": "application/json; charset=UTF-8",
                    "Access-Control-Allow-Origin": "*"
                  },
                  body: JSON.stringify({
                    recipe_id: JSON.parse(data.body)[0].id,
                    items: items
                  }),
                  mode: "cors"
                }).then(async (resp) => await resp.text())
              })
        });
  }
}

// suggest the items based on the value of the searchbar input
async function getItemsFromLetters(letters) {
  return await fetch(ENDPOINT_SERVER + "/item/guess/" + letters, {
    headers: {
      "Accept": "application/json; charset=UTF-8",
      "Response-type": "application/json; charset=UTF-8",
      "Content-Type": "application/json; charset=UTF-8",
      "Access-Control-Allow-Origin": "*"
    },
    method: "GET",
    mode: "cors"
  }).then((resp) => resp.json());
}

window.addEventListener("popstate", router);

document.addEventListener("DOMContentLoaded", () => {
  document.body.addEventListener("click", e => {
    if (e.target.matches("[data-link]")) {
      e.preventDefault();
      navigateTo(e.target.href);
    }
  });

  router();

  const nav_links = document.getElementsByClassName("nav__link");

  // make navigation elements visibly active
  var current = document.getElementsByClassName("active");
  if (current.length == 0) {
    for (var i = 0; i < nav_links.length; i++) {
      if (nav_links[i].pathname == location.pathname) {
        nav_links[i].className += " active";
        break;
      }
    }
  }

  for (var i = 0; i < nav_links.length; i++) {
    nav_links[i].addEventListener("click", function () {
      current = document.getElementsByClassName("active");

      if (current.length > 0) {
        current[0].className = current[0].className.replace(" active", "");
      }

      this.className += " active";
    });
  }
});

